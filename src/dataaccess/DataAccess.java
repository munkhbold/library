package dataaccess;

import java.util.HashMap;
import java.util.HashSet;

import business.Author;
import business.Book;
import business.CheckoutRecord;
import business.LibraryMember;
import dataaccess.DataAccessFacade.StorageType;

public interface DataAccess { 
	public HashMap<String,Book> readBooksMap();
	public HashMap<String,User> readUserMap();
	public HashMap<String, LibraryMember> readMemberMap();
	public void saveNewMember(LibraryMember member); 
	public void saveNewBook(Book book);
	public HashMap<String, CheckoutRecord> readCheckoutRecords();
	public void saveCheckoutRecord(CheckoutRecord record);
	public void saveUpdatedBook(Book book);
	public HashMap<String, Author> readAuthorMap();
	public void saveNewAuthor(Author author); 
}
