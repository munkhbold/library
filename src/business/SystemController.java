package business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dataaccess.Auth;
import dataaccess.DataAccess;
import dataaccess.DataAccessFacade;
import dataaccess.User;
import javafx.scene.control.TextField;

public class SystemController implements ControllerInterface {
	public static Auth currentAuth = null;
	public static User user = null;
	DataAccess da;

	public SystemController() {
		da = new DataAccessFacade();
	}
	
	public void login(String id, String password) throws LoginException {
		HashMap<String, User> map = da.readUserMap();
		if(!map.containsKey(id)) {
			throw new LoginException("ID " + id + " not found");
		}
		String passwordFound = map.get(id).getPassword();
		if(!passwordFound.equals(password)) {
			throw new LoginException("Password incorrect");
		}
		currentAuth = map.get(id).getAuthorization();
		user = map.get(id);
		
	}

	@Override
	public List<Author> allAuthors() {
		return new ArrayList<>(da.readAuthorMap().values());
	}

	@Override
	public List<String> allMemberIds() {
		List<String> retval = new ArrayList<>();
		retval.addAll(da.readMemberMap().keySet());
		return retval;
	}

	@Override
	public List<LibraryMember> allMembers() {
		return new ArrayList<>(da.readMemberMap().values());
	}
	
	@Override
	public List<String> allBookIds() {
		List<String> retval = new ArrayList<>();
		retval.addAll(da.readBooksMap().keySet());
		return retval;
	}
	
	@Override
	public List<CheckoutRecord> allCheckoutRecords() {
		return new ArrayList<>(da.readCheckoutRecords().values());
	}
	

	@Override
	public Book getBookByIsbn(String isbn) {
		return da.readBooksMap().getOrDefault(isbn, null);
	}
	
	@Override
	public List<Book> allBooks() {
		return new ArrayList<>(da.readBooksMap().values());
	}

	@Override
	public Book createBook(String isbn, String title, int maxLen, List<Author> authors, int numOfCopy) {
		Book book = new Book(isbn, title, maxLen, authors);
		if(numOfCopy> 0)
			for(int i=1; i<numOfCopy; i++) {
				book.addCopy();
			}
		da.saveNewBook(book);
		return book;
	}
	
	private String createUniqueMemberId() {
		List<String> l = new ArrayList<>(da.readMemberMap().keySet());
		Collections.sort(l);
		String id = "";
		if(l.size() > 0) {
			id = "" + (Integer.parseInt(l.get(l.size()-1)) + 1);
		}
		return id;
	}

	@Override
	public LibraryMember createMember(String firstname, String lastname, String tel, String city, String state, String zip, String street) {
		// TODO: check address by zip
		Address a = new Address(street, city, state, zip);
		String memberId = createUniqueMemberId();
		LibraryMember member = new LibraryMember(memberId, firstname, lastname, tel, a);
		da.saveNewMember(member);
		return member;
	}
	
	@Override
	public LibraryMember updateMember(String id, String firstname, String lastname, String tel, String city, String state, String zip, String street) {
		Address a = new Address(street, city, state, zip);
		LibraryMember member = new LibraryMember(id, firstname, lastname, tel, a);
		da.saveNewMember(member);
		return member;
	}
	
	@Override
	public CheckoutEntry loadCheckoutEntry(String bookId) {
		// TODO: Save checkout entry in file
		for(CheckoutRecord record: da.readCheckoutRecords().values()) {
			for(CheckoutEntry entry: record.getEntries()) {
				Book book = entry.getBookcopy().getBook();
				if(book.getIsbn().equals(bookId)) {
					return entry;
				}
			}
		}
		return null;
	}

	@Override
	public LibraryMember getMemberById(String memberId) {
		return da.readMemberMap().getOrDefault(memberId, null);
	}

	@Override
	public void addCheckoutEntry(String isbn, String memberId) {
		LibraryMember member = da.readMemberMap().getOrDefault(memberId, null);
		Book book = da.readBooksMap().getOrDefault(isbn, null);
		BookCopy bookCopy = book.getNextAvailableCopy();
		
		// update status
		bookCopy.changeAvailability();
		book.updateCopies(bookCopy);
		da.saveUpdatedBook(book);

		CheckoutRecord record = da.readCheckoutRecords().getOrDefault(memberId, null);
		if(record == null) {
			record = new CheckoutRecord(member, bookCopy, book.getMaxCheckoutLength());
		}else {
			record.addEntry(bookCopy, book.getMaxCheckoutLength());			
		}
		
		da.saveCheckoutRecord(record);
	}

	@Override
	public Author createAuthor(String firstname, String lastname, String bio, String tel, String city,
			String state, String zip, String street) {
		// TODO: check address by zip
		Address a = new Address(street, city, state, zip);
		Author author = new Author(firstname, lastname, tel, a, bio);
		da.saveNewAuthor(author);
		return author;
		
	}
	
}
