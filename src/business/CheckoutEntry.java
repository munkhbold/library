package business;

import java.io.Serializable;
import java.time.LocalDate;

public class CheckoutEntry implements Serializable{
	private LocalDate dueDate;
	private LocalDate issuedDate;
	private BookCopy bookcopy;
	private int days;
	
	public CheckoutEntry(BookCopy bookcopy, int days) {
		LocalDate now = LocalDate.now();
		this.issuedDate = now;
		this.dueDate = now.plusDays(days);
		this.days = days;
		this.bookcopy = bookcopy;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public int getDays() {
		return days;
	}

	public BookCopy getBookcopy() {
		return bookcopy;
	}
	
	public boolean equals(Object ob) {
		if (ob == null) return false;
		if (!(ob instanceof CheckoutEntry)) {
			return false;
		}
		CheckoutEntry ce = (CheckoutEntry) ob;
		return this.bookcopy.equals(ce.getBookcopy());
	}
	
	public LocalDate getIssuedDate() {
		return issuedDate;
	}

	private static final long serialVersionUID = -7848324024743486461L;
}
