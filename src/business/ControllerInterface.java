package business;

import java.util.List;

import business.Book;
import dataaccess.DataAccess;
import dataaccess.DataAccessFacade;
import javafx.scene.control.TextField;

public interface ControllerInterface {
	public void login(String id, String password) throws LoginException;
	public List<String> allMemberIds();
	public List<String> allBookIds();
	public List<Book> allBooks();
	public Book createBook(String isbn, String title, int maxLen, List<Author> authors, int numOfCopy);
	public List<LibraryMember> allMembers();
	public LibraryMember createMember(String firstname, String lastname, String tel, 
			String city, String state, String zip, String street);
	public Book getBookByIsbn(String isbn);
	public LibraryMember getMemberById(String memberId);
	public void addCheckoutEntry(String isbn, String memberId);
	public CheckoutEntry loadCheckoutEntry(String bookId);
	public List<CheckoutRecord> allCheckoutRecords();
	public List<Author> allAuthors();
	public Author createAuthor(String firstname, String lastname, String bio, String tel, String city,
			String state, String zip, String street);
	public LibraryMember updateMember(String id, String firstname, String lastname, String tel, String city, String state,
			String zip, String street);
	
	
}
