package business;

import java.io.Serializable;
import java.util.UUID;

final public class Author extends Person implements Serializable {
	private String bio;
	private String id;
	public String getBio() {
		return bio;
	}
	
	public Author(String f, String l, String t, Address a, String bio) {
		super(f, l, t, a);
		this.bio = bio;
		this.id = UUID.randomUUID().toString();
		
	}
	
	@Override
	public String toString() {
		return this.getFirstName() + " " + this.getLastName();
	}

	private static final long serialVersionUID = 7508481940058530471L;

	public String getId() {
		return id;
	}
}
