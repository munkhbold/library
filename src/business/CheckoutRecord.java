package business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CheckoutRecord implements Serializable{
	private LibraryMember member;
	private List<CheckoutEntry> entries;
	
	public CheckoutRecord(LibraryMember member, BookCopy book, int days) {
		this.member = member;
		entries = new ArrayList<>();
		this.addEntry(book, days);
	}
	
	public void addEntry(BookCopy book, int days) {
		entries.add(new CheckoutEntry(book, days));
	}
	
	public LibraryMember getMember() {
		return member;
	}
	
	public List<CheckoutEntry> getEntries() {
		return entries;
	}


	private static final long serialVersionUID = -7683781252477823248L;
}
