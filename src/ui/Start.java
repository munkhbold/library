package ui;

import javafx.application.Application;
import javafx.stage.Stage;
import ui.bookAdd.BookAddWindow;
import ui.bookList.BookWindow;
import ui.checkout.CheckoutWindow;
import ui.memberList.MemberWindow;
import ui.userLogin.LoginWindow;


public class Start extends Application {
	public static void main(String[] args) {
		launch(args);
	}
	private static Stage primStage = null;
	public static Stage primStage() {
		return primStage;
	}
	
	private static Stage[] allWindows = { 
		LoginWindow.INSTANCE,
		BookWindow.INSTANCE
	};
	
	public static void hideAllWindows() {
		primStage.hide();
		for(Stage st: allWindows) {
			st.hide();
		}
	}
	
	@Override
	public void start(Stage primaryStage) {
		primStage = primaryStage;
		
		if(!LoginWindow.INSTANCE.isInitialized()) {
			LoginWindow.INSTANCE.init();
		}

		LoginWindow.INSTANCE.show();

	}
	

}
