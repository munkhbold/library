package ui.help;


import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ui.LibWindow;

public class HelpWindow extends Stage implements LibWindow {
	public static final HelpWindow INSTANCE = new HelpWindow();
	private boolean isInitialized = false;
	public boolean isInitialized() {
		return isInitialized;
	}
	public void isInitialized(boolean val) {
		isInitialized = val;
	}
	/* This class is a singleton */
	private HelpWindow() {}
	
	public void display() {
		if(!isInitialized()) 
			init();

		this.show();	
	}

	public void init() {
		try {

			FXMLLoader fxmlRoot = new FXMLLoader(getClass().getResource("About.fxml"));     
	        Parent root = fxmlRoot.load();
	        Scene scene = new Scene(root);
	        scene.getStylesheets().add(getClass().getResource("../../css/dark.css").toExternalForm());
	        setScene(scene);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
