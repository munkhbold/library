package ui.bookList;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import business.Author;
import business.Book;
import business.ControllerInterface;
import business.SystemController;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import ui.bookAdd.BookAddWindow;
import ui.checkout.CheckoutWindow;

public class BookListControl implements Initializable{
    ObservableList<BookRow> list = FXCollections.observableArrayList();

	@FXML
    private StackPane rootPane;
    @FXML
    private TableView<BookRow> tableView;
    @FXML
    private TableColumn<BookRow, String> titleCol;
    @FXML
    private TableColumn<BookRow, String> idCol;
    @FXML
    private TableColumn<BookRow, String> authorCol;
    @FXML
    private TableColumn<BookRow, String> maxLenCol;
    @FXML
    private TableColumn<BookRow, Boolean> availabiltyCol;
    @FXML
    private TableColumn<BookRow, String> copyCol;
    @FXML
    private AnchorPane contentPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initCol();
        loadData();
    }

    private void initCol() {
        titleCol.setCellValueFactory(new PropertyValueFactory<>("title"));
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        authorCol.setCellValueFactory(new PropertyValueFactory<>("author"));
        maxLenCol.setCellValueFactory(new PropertyValueFactory<>("maxLen"));
        availabiltyCol.setCellValueFactory(new PropertyValueFactory<>("availabilty"));
        copyCol.setCellValueFactory(new PropertyValueFactory<>("copies"));
    }
    
    public void loadData() {
		ControllerInterface ci = new SystemController();
		List<Book> books = ci.allBooks();
		for(Book b: books) {
			String authorCol = "";
			List<Author> authorList = b.getAuthors();
			if (authorList.size() > 0) {
				int len = authorList.size()-1;
				for(int i=0; i<len; i++) {
					if(authorList.get(i)!=null)
						authorCol += authorList.get(i).getFirstName() + ", ";
				}
				
				if(authorList.get(len)!=null)
					authorCol += authorList.get(len).getFirstName();
			}

			list.add(new BookRow(b.getTitle(), b.getIsbn(), authorCol, "" + b.getMaxCheckoutLength(), b.isAvailable(), "" + b.getCopies().length));
		}
		tableView.setItems(list);
    }
    
    public static class BookRow {

        private final SimpleStringProperty title;
        private final SimpleStringProperty id;
        private final SimpleStringProperty author;
        private final SimpleStringProperty maxLen;
        private final SimpleStringProperty availabilty;
        private final SimpleStringProperty copies;

        public BookRow(String title, String id, String author, String maxLen, Boolean avail, String copies) {
            this.title = new SimpleStringProperty(title);
            this.id = new SimpleStringProperty(id);
            this.author = new SimpleStringProperty(author);
            this.maxLen = new SimpleStringProperty(maxLen);
            if (avail) {
                this.availabilty = new SimpleStringProperty("Available");
            } else {
                this.availabilty = new SimpleStringProperty("Issued");
            }
            this.copies = new SimpleStringProperty(copies);
        }

        public String getTitle() {
            return title.get();
        }

        public String getId() {
            return id.get();
        }

        public String getAuthor() {
            return author.get();
        }

        public String getMaxLen() {
            return maxLen.get();
        }

        public String getAvailabilty() {
            return availabilty.get();
        }
        
        public String getCopies() {
            return copies.get();
        }

    }
    
    @FXML
    private void actionRefresh(ActionEvent event) {
    	tableView.getItems().clear();
    	loadData();
    }
    
    @FXML
    private void actionGoBack(ActionEvent event) {
    	BookWindow.INSTANCE.hide();
    	CheckoutWindow.INSTANCE.show();
    }
    
}
