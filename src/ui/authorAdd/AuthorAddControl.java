package ui.authorAdd;


import business.ControllerInterface;
import business.SystemController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class AuthorAddControl {
	@FXML TextField fieldLastname, fieldFirstname, fieldTel, fieldCity, fieldState, fieldZip, fieldStreet;
	@FXML TextArea fieldBio;
	
    @FXML
    private void actionSave(ActionEvent event) {
    	ControllerInterface ci = new SystemController();
    	String firstname = fieldFirstname.getText();
    	
    	String lastname = fieldLastname.getText();
    	String bio = fieldBio.getText();
    	String tel = fieldTel.getText();
    	String city = fieldCity.getText();
    	String state = fieldState.getText();
    	String zip = fieldZip.getText();
    	String street = fieldStreet.getText();
    	
    	
    	if( firstname.isEmpty() || lastname.isEmpty() || tel.isEmpty() || city.isEmpty() || state.isEmpty() || zip.isEmpty() || street.isEmpty()) {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
    		alert.setHeaderText(null);
    		alert.setContentText("Please enter in all fields");
    		alert.showAndWait();
    		return;
    	}
    		
    	ci.createAuthor(firstname, lastname, bio, tel, city, state, zip, street);
    	AuthorAddWindow.INSTANCE.hide();
    }

    @FXML
    private void actionCancel(ActionEvent event) {
		AuthorAddWindow.INSTANCE.hide();
    }
}
