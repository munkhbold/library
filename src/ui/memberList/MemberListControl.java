package ui.memberList;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import business.Address;
import business.ControllerInterface;
import business.LibraryMember;
import business.SystemController;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import ui.checkout.CheckoutWindow;
import ui.memberAdd.MemberAddWindow;

public class MemberListControl implements Initializable{
    ObservableList<MemberRow> list = FXCollections.observableArrayList();

	@FXML
    private StackPane rootPane;
    @FXML
    private TableView<MemberRow> tableView;
    @FXML
    private TableColumn<MemberRow, String> idCol;
    @FXML
    private TableColumn<MemberRow, String> firstnameCol;
    @FXML
    private TableColumn<MemberRow, String> lastnameCol;
    @FXML
    private TableColumn<MemberRow, String> telCol;
    @FXML
    private TableColumn<MemberRow, String> cityCol;
    @FXML
    private TableColumn<MemberRow, String> stateCol;
    @FXML
    private TableColumn<MemberRow, String> zipCol;
    @FXML
    private TableColumn<MemberRow, String> streetCol;
    @FXML
    private AnchorPane contentPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initCol();
        loadData();
    }

    private void initCol() {
    	idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
    	firstnameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
    	lastnameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
    	telCol.setCellValueFactory(new PropertyValueFactory<>("tel"));
    	cityCol.setCellValueFactory(new PropertyValueFactory<>("city"));
    	stateCol.setCellValueFactory(new PropertyValueFactory<>("state"));
    	zipCol.setCellValueFactory(new PropertyValueFactory<>("zip"));
    	streetCol.setCellValueFactory(new PropertyValueFactory<>("street"));
    }
    
    public void loadData() {
		ControllerInterface ci = new SystemController();
		
		List<LibraryMember> members = ci.allMembers();
		for(LibraryMember b: members) {
			Address a = b.getAddress();
			list.add(new MemberRow(b.getMemberId(), 
									b.getFirstName(),
									b.getLastName(),
									b.getTelephone(),
									a.getCity(),
									a.getState(),
									a.getZip(),
									a.getStreet()));
		}
		tableView.setItems(list);
    }
    
    public static class MemberRow {

    	private final SimpleStringProperty id;
        private final SimpleStringProperty firstname;
        private final SimpleStringProperty lastname;
        private final SimpleStringProperty tel;
        private final SimpleStringProperty city;
        private final SimpleStringProperty state;
        private final SimpleStringProperty zip;
        private final SimpleStringProperty street;

        public MemberRow(String id, String firstname, String lastname, 
        		String tel, String city, String state, String zip, String street) {
        	this.id = new SimpleStringProperty(id);
        	this.firstname = new SimpleStringProperty(firstname);
            this.lastname = new SimpleStringProperty(lastname);
            this.tel = new SimpleStringProperty(tel);
            this.city = new SimpleStringProperty(city);
            this.state = new SimpleStringProperty(state);
            this.zip = new SimpleStringProperty(zip);
            this.street = new SimpleStringProperty(street);
        }

        public String getId() {
            return id.get();
        }

        public String getFirstName() {
            return firstname.get();
        }

        public String getLastName() {
            return lastname.get();
        }

        public String getCity() {
            return city.get();
        }

        public String getState() {
            return state.get();
        }
        
        public String getTel() {
            return tel.get();
        }

        public String getZip() {
            return zip.get();
        }
        
        public String getStreet() {
            return street.get();
        }

    }
    
    @FXML
    private void actionEditMember(ActionEvent event) {
    	MemberRow member = tableView.getSelectionModel().getSelectedItem();
    	if(member != null) {
    		MemberAddWindow.INSTANCE.editWindow(member.getId());
    		return;
    	}
    	Alert alert = new Alert(Alert.AlertType.WARNING);
		alert.setHeaderText(null);
		alert.setContentText("Please select member!");
		alert.showAndWait();
    }
    
    @FXML
    private void actionRefresh(ActionEvent event) {
    	tableView.getItems().clear();
    	loadData();
    }
    
    @FXML
    private void actionGoBack(ActionEvent event) {
    	MemberWindow.INSTANCE.hide();
    	CheckoutWindow.INSTANCE.show();
    }
    
}
