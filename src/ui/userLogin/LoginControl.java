package ui.userLogin;


import business.ControllerInterface;
import business.LoginException;
import business.SystemController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import ui.checkout.CheckoutWindow;

public class LoginControl{

    @FXML
    private TextField fieldUsername, fieldPassword;

    @FXML
    private void login(ActionEvent event) {
		try {
			ControllerInterface c = new SystemController();
			c.login(fieldUsername.getText().trim(), fieldPassword.getText().trim());
			
			
			if(!CheckoutWindow.INSTANCE.isInitialized()) {
				CheckoutWindow.INSTANCE.init();
			}
			LoginWindow.INSTANCE.hide();
			CheckoutWindow.INSTANCE.show();

		} catch(LoginException ex) {
    		Alert alert = new Alert(Alert.AlertType.WARNING);
    		alert.setHeaderText(null);
    		alert.setContentText("Login failed! Invalid username or password");
    		alert.showAndWait();

			System.out.println("Error! " + ex.getMessage());
		}
    }

}
