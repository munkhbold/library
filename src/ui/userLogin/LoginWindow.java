package ui.userLogin;


import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ui.LibWindow;

public class LoginWindow extends Stage implements LibWindow {
	public static final LoginWindow INSTANCE = new LoginWindow();
	private boolean isInitialized = false;
	
	public boolean isInitialized() {
		return isInitialized;
	}
	public void isInitialized(boolean val) {
		isInitialized = val;
	}
	
	/* This class is a singleton */
    private LoginWindow () {}
    
    public void init() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
	        Scene scene = new Scene(root);
	        setScene(scene);
		} catch(Exception e) {
			e.printStackTrace();
		}
        
    }
}
