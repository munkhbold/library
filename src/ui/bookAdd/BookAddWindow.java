package ui.bookAdd;


import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ui.LibWindow;

public class BookAddWindow extends Stage implements LibWindow {
	public static final BookAddWindow INSTANCE = new BookAddWindow();
	
	private boolean isInitialized = false;
	public boolean isInitialized() {
		return isInitialized;
	}
	public void isInitialized(boolean val) {
		isInitialized = val;
	}
	/* This class is a singleton */
	private BookAddWindow() {}
	
	public void display() {
		if(!isInitialized()) 
			init();

		this.show();	
	}
	
	public void init() {
		try {

	        Parent root = FXMLLoader.load(getClass().getResource("BookAdd.fxml"));
	        Scene scene = new Scene(root);
	        scene.getStylesheets().add(getClass().getResource("../../css/dark.css").toExternalForm());
	        setScene(scene);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
