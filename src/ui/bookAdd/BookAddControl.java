package ui.bookAdd;


import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;

import business.Author;
import business.ControllerInterface;
import business.SystemController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import ui.authorAdd.AuthorAddWindow;
import ui.bookList.BookWindow;

public class BookAddControl implements Initializable{
	HashSet<Author> authorSet = new HashSet<>();
	@FXML TextField fieldTitle, fieldISBN, fieldAuthor, fieldMaxLen, fieldCopies;
	
	@FXML ComboBox<Author> comboAuthor;
    @FXML
    private void actionSave(ActionEvent event) {
    	ControllerInterface ci = new SystemController();
    	String title = fieldTitle.getText();
    	String isbn = fieldISBN.getText();
    	String maxLen = fieldMaxLen.getText();
    	String numOfCopy = fieldCopies.getText();
    	
    	if( title.isEmpty() || isbn.isEmpty() || maxLen.isEmpty() || authorSet.size() <= 0) {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
    		alert.setHeaderText(null);
    		alert.setContentText("Please enter in all fields");
    		alert.showAndWait();
    		return;
    	}
    	
    	List<Author> authors = new ArrayList<>(authorSet);
    	
    	int copyDigit = Integer.parseInt(numOfCopy.isEmpty() ? "0" : numOfCopy);
    	ci.createBook(isbn, title, Integer.parseInt(maxLen), authors, copyDigit);
    	BookWindow.INSTANCE.display();
    	BookAddWindow.INSTANCE.hide();
    }

    @FXML
    private void actionCancel(ActionEvent event) {
		BookAddWindow.INSTANCE.hide();
    }

    @FXML
    private void actionAddAuthor(ActionEvent event) {
    	AuthorAddWindow.INSTANCE.display();
    }
    

    @FXML
    private void actionClearAuthor(ActionEvent event) {
    	authorSet.clear();
    	authorSet = new HashSet<>();
    	fieldAuthor.setText("");
    	loadAuthors();
    }
    
    
    @FXML
    private void actionSelectAuthor(ActionEvent event) {
    	Author author = comboAuthor.getValue();
    	authorSet.add(author);
    	
    	String names = "";
    	if (authorSet.size() <= 0)
    		return;

    	for(Author a: authorSet) {
    		if(a != null)
    			names += a.getFirstName() + "" + a.getLastName() + ", ";
    	}
    	fieldAuthor.setText(names);
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadAuthors();
	}
	
	private void loadAuthors() {
    	ControllerInterface ci = new SystemController();
        ObservableList<Author> list = FXCollections.observableArrayList(ci.allAuthors());
        comboAuthor.setItems(list);

    }
}
