package ui.memberAdd;


import business.Address;
import business.ControllerInterface;
import business.LibraryMember;
import business.SystemController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class MemberAddControl {
	String memberId = null;
	@FXML TextField fieldLastname, fieldFirstname, fieldTel, fieldCity, fieldState, fieldZip, fieldStreet;
	
    @FXML
    private void actionSave(ActionEvent event) {
    	ControllerInterface ci = new SystemController();
    	String firstname = fieldFirstname.getText();
    	
    	String lastname = fieldLastname.getText();
    	String tel = fieldTel.getText();
    	String city = fieldCity.getText();
    	String state = fieldState.getText();
    	String zip = fieldZip.getText();
    	String street = fieldStreet.getText();
    	
    	
    	if( firstname.isEmpty() || lastname.isEmpty() || tel.isEmpty() || city.isEmpty() || state.isEmpty() || zip.isEmpty() || street.isEmpty()) {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
    		alert.setHeaderText(null);
    		alert.setContentText("Please enter in all fields");
    		alert.showAndWait();
    		return;
    	}
    	
    	if(memberId!=null) {
    		ci.updateMember(memberId, firstname, lastname, tel, city, state, zip, street);
    		memberId = null;
    	}else {
    		ci.createMember(firstname, lastname, tel, city, state, zip, street);	
    	}
    	
    	MemberAddWindow.INSTANCE.hide();
    }

    @FXML
    private void actionCancel(ActionEvent event) {
		MemberAddWindow.INSTANCE.hide();
    }
    
    public void memberEdit(LibraryMember member) {
    	memberId = member.getMemberId();
		fieldFirstname.setText(member.getFirstName());
		fieldLastname.setText(member.getLastName());
		fieldTel.setText(member.getTelephone());
		
		Address adr = member.getAddress();
		if(adr != null) {
			fieldCity.setText(adr.getCity());
			fieldState.setText(adr.getState());
			fieldZip.setText(adr.getZip());
			fieldStreet.setText(adr.getStreet());
			
		}
	}
}
