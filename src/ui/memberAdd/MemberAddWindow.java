package ui.memberAdd;


import business.LibraryMember;
import business.SystemController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ui.LibWindow;

public class MemberAddWindow extends Stage implements LibWindow {
	public static final MemberAddWindow INSTANCE = new MemberAddWindow();
	private FXMLLoader fxmlRoot;
	private boolean isInitialized = false;
	public boolean isInitialized() {
		return isInitialized;
	}
	public void isInitialized(boolean val) {
		isInitialized = val;
	}
	/* This class is a singleton */
	private MemberAddWindow() {}
	
	public void display() {
		if(!isInitialized()) 
			init();

		this.show();	
	}

	public void init() {
		try {

			fxmlRoot = new FXMLLoader(getClass().getResource("MemberAdd.fxml"));     
	        Parent root = fxmlRoot.load();
	        Scene scene = new Scene(root);
	        scene.getStylesheets().add(getClass().getResource("../../css/dark.css").toExternalForm());
	        setScene(scene);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void editWindow(String id) {
		if(!isInitialized()) 
			init();

		SystemController ci = new SystemController();
		LibraryMember member = ci.getMemberById(id);

		if(member == null)
			return;
			
		MemberAddControl controller = fxmlRoot.<MemberAddControl>getController();
		controller.memberEdit(member);
		
		this.show();

	}
}
