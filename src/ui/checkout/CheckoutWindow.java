package ui.checkout;


import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ui.LibWindow;

public class CheckoutWindow extends Stage implements LibWindow {
	public static final CheckoutWindow INSTANCE = new CheckoutWindow();
	
	private boolean isInitialized = false;
	public boolean isInitialized() {
		return isInitialized;
	}
	public void isInitialized(boolean val) {
		isInitialized = val;
	}
	/* This class is a singleton */
	private CheckoutWindow() {}
	
	public void display() {
		if(!isInitialized()) 
			init();

		this.show();	
	}
	
	public void init() {
		try {

	        Parent root = FXMLLoader.load(getClass().getResource("CheckoutTemplate.fxml"));
	        Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("../../css/dark.css").toExternalForm());
	        setScene(scene);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
