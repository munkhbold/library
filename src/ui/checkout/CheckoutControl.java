package ui.checkout;

import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import business.Book;
import business.CheckoutEntry;
import business.CheckoutRecord;
import business.ControllerInterface;
import business.LibraryMember;
import business.SystemController;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import ui.bookAdd.BookAddWindow;
import ui.bookList.BookWindow;
import ui.help.HelpWindow;
import ui.memberAdd.MemberAddWindow;
import ui.memberList.MemberWindow;
import ui.userLogin.LoginWindow;

public class CheckoutControl implements Initializable{
    private static final String BOOK_NOT_AVAILABLE = "Not Available";
    private static final String NO_SUCH_BOOK_AVAILABLE = "No Such Book Available";
    private static final String NO_SUCH_MEMBER_AVAILABLE = "No Such Member Available";
    private static final String BOOK_AVAILABLE = "Available";
    
	@FXML private TextField textBookid, textMemberid;
	@FXML private Label lblCurrentUser;
	// using on records tab
	@FXML private TextField entryBookId;
	@FXML private Text lblBookName, lblBookAuthor, lblBookStatus;
	@FXML private Text lblMemberName, lblMemberTel;
	@FXML private Button btnAddMember, btnAddBook;

	
    ObservableList<CheckoutRecordRow> list = FXCollections.observableArrayList();

    @FXML private TableView<CheckoutRecordRow> tableRecordView;
    @FXML private TableColumn<CheckoutRecordRow, String> memberCol;
    @FXML private TableColumn<CheckoutRecordRow, String> bookCol;
    @FXML private TableColumn<CheckoutRecordRow, String> issueDateCol;
    @FXML private TableColumn<CheckoutRecordRow, String> dueDateCol;
    @FXML private TableColumn<CheckoutRecordRow, String> overdueCol;
    
	private ControllerInterface ci;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ci = new SystemController();
		initCheckoutRecords();
		loadRecordsData();
		lblCurrentUser.setText("Welcome " + SystemController.user.getId());
		
		if(!SystemController.user.isAdmin()) {
			btnAddBook.setVisible(false);
			btnAddMember.setVisible(false);
		}
		
	}
	
	private void clearMemberCache() {
		lblMemberName.setText("");
		lblMemberTel.setText("");
	}
	
	private void clearBookCache() {
		lblBookName.setText("");
		lblBookAuthor.setText("");
		lblBookStatus.setText("");
		
	}
	private void clearEntries() {
		textBookid.setText("");
		textMemberid.setText("");
		
		lblMemberName.setText("Member Name");
		lblMemberTel.setText("Contact");
		lblBookName.setText("Book Name");
		lblBookAuthor.setText("Author");
		lblBookStatus.setText("Status");
	}
	
	void alert(Alert.AlertType type, String message) {
    	Alert alert = new Alert(type);
		alert.setHeaderText(null);
		alert.setContentText(message);
		alert.showAndWait();
    }

    @FXML
    public void actionExit(ActionEvent event) {
    	Platform.exit();
    }

	@FXML
    public void actionLogout(ActionEvent event) {
		if(!LoginWindow.INSTANCE.isInitialized()) {
			LoginWindow.INSTANCE.init();
		}
		
		CheckoutWindow.INSTANCE.hide();
		LoginWindow.INSTANCE.show();
    }

    @FXML
    public void actionLoadMembers(ActionEvent event) {
    	MemberWindow.INSTANCE.display();
    }
    
    @FXML
    public void actionAddMember(ActionEvent event) {
    	if(!SystemController.user.isAdmin()) {
    		alert(Alert.AlertType.WARNING, "Permission denied");
    		return;
    	}

		MemberAddWindow.INSTANCE.display();
    }
    
    @FXML
    public void actionAddBook(ActionEvent event) {
    	if(!SystemController.user.isAdmin()) {
    		alert(Alert.AlertType.WARNING, "Permission denied");
    		return;
    	}

		BookAddWindow.INSTANCE.display();
    }
    
    @FXML
    public void actionLoadBooks(ActionEvent event) {
		BookWindow.INSTANCE.display();
    }
    
    @FXML
    public void actionAbout(ActionEvent event) {
		HelpWindow.INSTANCE.display();
    }
    
    @FXML
    public void actionLoadBookInfo(ActionEvent event) {
    	clearBookCache();
    	String isbn = textBookid.getText();
    	Book book = ci.getBookByIsbn(isbn);

    	if(book == null) {
    		lblBookName.setText(NO_SUCH_BOOK_AVAILABLE);
    		return;
    	}
    	
    	lblBookName.setText(book.getTitle());
    	lblBookAuthor.setText(book.getAuthorsNames());
    	lblBookStatus.setText(book.isAvailable() ? BOOK_AVAILABLE : BOOK_NOT_AVAILABLE);
    }
    
    @FXML
    public void actionLoadMemberInfo(ActionEvent event) {
    	clearMemberCache();
    	String memberId = textMemberid.getText();
    	
    	LibraryMember member = ci.getMemberById(memberId);

    	if(member == null) {
    		lblMemberName.setText(NO_SUCH_MEMBER_AVAILABLE);
    		return;
    	}
    	
    	lblMemberName.setText(String.format("%s %s", member.getFirstName(), member.getLastName()));
    	lblMemberTel.setText(member.getTelephone());
    }
    
    private void validateMatch(String bookId, String memberId) throws Exception {
    	Book book = ci.getBookByIsbn(bookId);
    	if(book == null)
    		throw new Exception(NO_SUCH_BOOK_AVAILABLE);
    	else if(!book.isAvailable())
    		throw new Exception(BOOK_NOT_AVAILABLE);
    	
    	LibraryMember member = ci.getMemberById(memberId);
    	if(member == null)
    		throw new Exception(NO_SUCH_MEMBER_AVAILABLE);
    	
    }
    

    @FXML
    public void actionCreateIssue(ActionEvent event) {
    	if(!SystemController.user.isLibrarian()) {
    		alert(Alert.AlertType.WARNING, "Permission denied");
    		return;
    	}
    		
    	String bookId = textBookid.getText();
    	String memberId = textMemberid.getText();
    	
    	try {
    		validateMatch(bookId, memberId);
    	}catch(Exception e) {
    		alert(Alert.AlertType.WARNING, e.getMessage());
    		return;
    	}

		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setHeaderText(null);
		alert.setContentText("Are you sure want to issue the book " + lblBookName.getText() + "\n to " + lblMemberName.getText() + " ?");
		Optional<ButtonType> response = alert.showAndWait();

		if(response.get() == ButtonType.OK) {
			ci.addCheckoutEntry(bookId, memberId);
			loadRecordsData();
			clearEntries();
		}
	}
    
    public void loadRecordsData() {
    	// clear table items before load data
    	tableRecordView.getItems().clear();
		List<CheckoutRecord> records = ci.allCheckoutRecords();
		for(CheckoutRecord record: records) {
			String memberId = record.getMember().getMemberId();

			for(CheckoutEntry entry: record.getEntries()) {
				String bookId = entry.getBookcopy().getBook().getIsbn();
				LocalDate issuedDate = entry.getIssuedDate();
				LocalDate dueDate = entry.getDueDate();
				
				long overdueDays = 0;
				LocalDate now = LocalDate.now();
				if(dueDate.compareTo(now) < 0) {
					overdueDays = ChronoUnit.DAYS.between(now, dueDate);
				}
				list.add(new CheckoutRecordRow(memberId, bookId, issuedDate, dueDate, overdueDays));
			}
		}
		
		tableRecordView.setItems(list);
    }


    private void initCheckoutRecords() {
    	memberCol.setCellValueFactory(new PropertyValueFactory<>("memberId"));
    	bookCol.setCellValueFactory(new PropertyValueFactory<>("bookId"));
    	issueDateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
    	dueDateCol.setCellValueFactory(new PropertyValueFactory<>("dueDate"));
    	overdueCol.setCellValueFactory(new PropertyValueFactory<>("overdue"));
    }

    public static class CheckoutRecordRow {

        private final SimpleStringProperty memberId;
        private final SimpleStringProperty bookId;
        private final SimpleStringProperty date;
        private final SimpleStringProperty dueDate;
        private final SimpleStringProperty overdue;

        public CheckoutRecordRow(String memberId, String bookId, LocalDate date, LocalDate dueDate, long overdue) {
            this.memberId = new SimpleStringProperty(memberId);
            this.bookId = new SimpleStringProperty(bookId);
            this.date = new SimpleStringProperty(date.toString());
            this.dueDate = new SimpleStringProperty(dueDate.toString());
            
            String overdueDays = overdue == 0 ? "" : Long.toString(overdue);
            this.overdue = new SimpleStringProperty(overdueDays);
        }

		public String getMemberId() {
			return memberId.get();
		}

		public String getBookId() {
			return bookId.get();
			
		}

		public String getDueDate() {
			return dueDate.get();
		}

		public String getDate() {
			return date.get();
		}
		
		public String getOverdue() {
			return overdue.get();
		}

    }
    
}
