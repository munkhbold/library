# Library

###Login
	1. After login you can see on top of the right user ID

###Checkout
	1. Insert book ISBN number in input and click enter. It'll show result on the right side of the input.
	2. Insert book Member ID in second input and click enter. It'll show result on the right side of the second input.
	3. Click issue button to create checkout entry
	4. Checkout records on next tab and you can see results

###Add member
	1. Click add member button on the right side of the panel and it'll show member form
	2. Click save button to insert member

###Add Book
	1. Click add book button on the right side of the panel and it'll show book form
	2. Click save button to insert book

###Book list
	1. Click books button and it'll show window that contains all books list
	2. Click refresh button if your change doesn't show
	3. Click go back to return main window

###Member list
	1. Click books button and it'll show window that contains all members list
	2. Click refresh button if your change doesn't show
	3. Click go back to return main window


###Logout
	1. You'll find logout on menu bar by selecting file dropdown
